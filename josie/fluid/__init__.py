""" Generic definitions for fluid dynamics systems """

from .fields import FluidFields
from .state import SingleFluidState
