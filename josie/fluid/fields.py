from josie.fields import Fields


class FluidFields(Fields):
    """Indexing fields for fluid systems"""

    U: int
    V: int
